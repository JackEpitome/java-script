const backpack = {
    name: "backpack",
    pocketnum: 20,
    capacity: 20,
    straps: {
        left_strap: 15,
        right_strap: 16,
    },
}

const markup = (backpack) => {
    return`
        <div>
            <section>
                <ul>
                    <li>name: ${backpack.name}</li>
                    <li>pocket numbers:${backpack.pocketnum}</li>
                    <li>capacity:${backpack.capacity}</li>
                    <li>left strap:${backpack.straps.left_strap}</li>
                    <li>right strap:${backpack.straps.right_strap}</li>
                    <li>strap total:${backpack.straps.right_strap + backpack.straps.left_strap}
                </ul>
            </section>
        </div>
`
}


const body = document.createElement("div");
body.innerHTML = markup(backpack);
document.body.appendChild(body);





// console.log (backpack)